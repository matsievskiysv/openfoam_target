### project settings

# base name to use for all files
GEOM_BASENAME=target

# project name
PROJECT=$(notdir $(realpath .))

# use MPI to run simulation in parallel mode. Possible values: yes | no
PARALLEL=no

# set time (e.g. time for VTK export). Possible values: <time dir name> | latestTime
TIME=latest

# number of processes to use with MPI. Generally, it should be derived automatically.
NPROC=$(shell foamDictionary -value -entry numberOfSubdomains system/decomposeParDict)

# solution log file name
LOGFILE=$(GEOM_BASENAME).log

# use stashed solution, when available (true|false)
USE_STASH=true

### -project settings


### freecad settings

# path to freecadcmd
FREECADCMD=freecadcmd

# freecad file
FREECAD_FILE=$(GEOM_BASENAME).FCStd

# freecad document (in a file)
FREECAD_DOCUMENT=target

# freecad bodies for step file exporting
FREECAD_BODIES=shell cooler heater

### -freecad settings

### gmsh settings

# path to gmsh
GMSH=gmsh

# input step file
GMSH_STEP=$(GEOM_BASENAME).step

# gmsh main script file
GMESH_FILE=$(GEOM_BASENAME).geo

# gmsh user configuration file
GMESH_CONFIG_FILE=$(GEOM_BASENAME)_config.geo

# output mesh file
MESH_FILE=$(GEOM_BASENAME).msh

# optimize mesh with netgen. Possible values: yes | no
OPTIMIZE_NETGEN=yes

### -gmsh settings

### paraview settings

# path to paraview
PARAVIEW=paraview

# paraview state file
PARAVIEW_STATE=$(wildcard $(GEOM_BASENAME).pvsm)

# paraview mesh preview files
PARAVIEW_MESH_VOLUMES=$(GEOM_BASENAME)_volumes.xdmf
PARAVIEW_MESH_SURFACES=$(GEOM_BASENAME)_surfaces.xdmf

### -paraview settings

### solver settings

# enable turbulence. Possible values: on | off
enableTurbulence=on

### -solver settings

### boundary conditions

# inlet mass flow rate [m^3/s]
inletFlowRate=2e-4

# outlet pressure [P]
outletPressure=1e5

# cooler temperature [K]
coolerT=300

# fixed boundary temperature [K]
fixedT=300

# initial temperature [K]
initialT=300

# gravity vector (x,y,z) [m/s^2]
gravity=0 0 -9.81

### -boundary conditions

### source parameters

# use geometric center or coordinate values switch (true|false)
coordinatesUseGeometricCenter=true

# X coordinate of the heating region center (Gaussian function) [m]. Ignored when coordinatesUseGeometricCenter is true
coordinateX=0

# Y coordinate of the heating region center (Gaussian function) [m]. Ignored when coordinatesUseGeometricCenter is true
coordinateY=0

# Z coordinate of the heating region center (Gaussian function) [m]. Ignored when coordinatesUseGeometricCenter is true
coordinateZ=0

# X axis standard deviation of the heating region (Gaussian function) [m]
sigmaX=0.001

# Y axis standard deviation of the heating region (Gaussian function) [m]
sigmaY=0.001

# Z axis standard deviation of the heating region (Gaussian function) [m]
sigmaZ=0.00025

# amplitude correction function. It is needed because Gaussian function is clipped by an application region
correctionFactor=1

# save heat source to file switch (true|false) (forced to false when PARALLEL=yes)
heatSourceSaveToFile=true

# save heat source file path
heatSourceFilePath=heatSourceData.csv

# number of time steps to keep on disk
writeCount=10

## source parameters: stationary

# stationary power source [W] (energy * current * impulseLength * frequency)
power=150

# save to disk each Nth iteration
writeInteration=10

# end of calculation iteration number
endIteration=1000

## -source parameters: stationary

## source parameters: transient

# energy of beam in transient mode [eV]
energy=1e+06

# current of beam in transient mode [A]
current=0.1

# beam impulse frequency [Hz]
frequency=100

# beam impulse length [s]
impulseLength=1e-05

# transient solution time step on impulse [s]
timeStepOn=1e-06

# transient solution time step off impulse [s]
timeStepOff=1e-02

# maximum Courant number. Must be less then 1
maxCo=0.5

# save to disk each Nth timestep
writeInterval=1e-05

# calculation end time [s]
endTime=1

## -source parameters: transient

### -source parameters
