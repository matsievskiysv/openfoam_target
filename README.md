# Electron accelerator target OpenFOAM calculation template

This is a template for calculation of electron accelerator target cooling using
[OpenFOAM](https://develop.openfoam.com/Development/openfoam/-/wikis/home) framework.

Unlike many other CAD programs, OpenFOAM is a collection of standalone CLI programs. OpenFOAM project consists of many files, which may be overwhelming to the new users. This project aims to simplify target cooling simulations by providing an initial set of files for the coupled thermo-hydrodynamic problem and a [GNU Make](https://www.gnu.org/software/make/) script to ensure that OpenFOAM programs are executed in a specific order.

This file describes how to reconfigure project and simulation for different target geometry and beam parameters. More detailed description of the project's internal structure is described in [internals.md](./.doc/internals.md) file.

Project uses the following programs:
  * [OpenFOAM](https://develop.openfoam.com/Development/openfoam/-/wikis/home) for thermodynamic-hydrodynamic coupled problem solution;
  * [FreeCAD](https://www.freecadweb.org) for target modeling;
  * [GMSH](https://gmsh.info/) for model meshing;
  * [ParaView](https://www.paraview.org/) for solution visualization;
  * [GNU Make](https://www.gnu.org/software/make/) for running commands in specified order.

All testing was done in [Debian](https://www.debian.org/) .

## Program installation

Most of the programs are installed using the following line:
```bash
sudo apt update && sudo apt install make freecad paraview python3-meshio
```

### GMSH

Project relies on GMSH mesh fields to set mesh sizes and on the option `IncludeBoundary` of the `Restrict` field. If this feature is already available from the repository, use this command to install GMSH:
```bash
sudo apt install gmsh
```

If it's not available, compile GMSH from sources:
```code
sudo apt update && sudo apt build-dep gmsh && sudo apt install fluid
git clone https://gitlab.onelab.info/gmsh/gmsh.git
cd gmsh
cmake -S . -B build -DCMAKE_INSTALL_PREFIX=/opt/gmsh
cmake --build build -j $(nproc)
sudo cmake --install build
echo 'export PATH=$PATH:/opt/gmsh/bin' >> $HOME/.profile
```

### OpenFOAM

At the moment of writing OpenFOAM Debian package didn't allow using `scalarCodedSourceCoeffs` function, necessary for distributed heating source definition, so we install OpenFOAM from sources.

Install build prerequisites:
```bash
sudo apt update && sudo apt build-dep openfoam
```

We will use stable OpenFOAM version:
```bash
mkdir -p $HOME/OpenFOAM && cd $HOME/OpenFOAM
git clone https://develop.openfoam.com/Development/openfoam.git --branch OpenFOAM-v2312 && cd openfoam
. ./etc/bashrc
```

OpenFOAM needs its environment to be set each time the program is used. In order to avoid setting it manually every time, issue the command
```bash
echo ". $(realpath ./etc/bashrc)" >> $HOME/.profile
```

Before start, check your system:
```bash
foamSystemCheck
```
This command's output should contain the line `System check: PASS`.

Start build:
```bash
foam
time ./Allwmake -j $(nproc)
```

To test installation, issue the command:
```bash
foamInstallationTest
```

**Note:** another installation instruction is available at [openFOAM repository](https://develop.openfoam.com/Development/openfoam/blob/develop/doc/Build.md).

## Workflow

One calculation iteration includes the following step:

1. 3D model preparation
2. Model meshing
3. Project variable configuration
4. Solution
5. Post-processing

These steps are repeated until target geometry is optimized.

Most of the project configurations, that are changed frequently during target optimization, are stored in [`config.mk`](./config.mk) file. These may be considered user configurations.

#### Variables

* `GEOM_BASENAME`: this is the base name used for most of the project files. Default: `target`.

### 3D model preparation

Any CAD modeler capable of exporting `.STEP` files may be used for model creation. We used using FreeCAD to create a target model.
Target consists of copper enclosure, tungsten target plate and water region. We will not go into detail about using FreeCAD because it is a subject on its own and there are plenty of resources dedicated to FreeCAD.

![model](.doc/model_freecad.png)
![water](.doc/model_freecad_water.png)

#### Variables

* `FREECAD_FILE`: FreeCAD file name.  Default: `$(GEOM_BASENAME).FCStd`.
* `FREECAD_DOCUMENT`: FreeCAD document name.  Default: `target`.
* `FREECAD_BODIES`: space separated list of 3D model bodies. Default: `water tungsten copper`
* `GMSH_STEP`: name of the resulting `.STEP` file. If you don't use FreeCAD, export you model in `.STEP` format with this name or set this variable to the name of your `.STEP` file in order for the GNU Make to use it. Default: `$(GEOM_BASENAME).step`

#### Commands

* `make target.step`: (in this case `GEOM_BASENAME` is `target`) export `FREECAD_BODIES` from `FREECAD_DOCUMENT` of `FREECAD_FILE`. Generally, this command should not be run manually.

### Model meshing

This step only requires initial configuration, after which it may be executed without `.GEO` script modification.

Initial configuration includes:

1. Model volume and surface name assignment
2. Mesh size assignment

#### Model volume and surface name assignment

`.STEP` files contain numbered volumes and surfaces. They should be matched to the project regions:

* `cooler`: cooler volume (e.g. water)
* `heater`: heater volume (e.g. tungsten)
* `shell`: shell volume (e.g. copper)
* `inlet`: cooler inlet surface
* `outlet`: cooler outlet surface
* `temperature`: constant temperature surface

Executing command `make gmsh-edit` will open GMSH window with imported geometry. In GMSH window click on `Tools->Visibility` to open Visibility window.

![gmsh](./.doc/gmsh_indices.png)

To learn surface and volume numbering in the Tree tab of the Visibility window select elements and click `Apply`. Elements with the selected numbers will be visible and all the others will get hidden.
Search for the required volumes and surfaces and fill `GMESH_CONFIG_FILE` variables with the appropriate values.

#### Mesh size assignment

When `.STEP` region names are matched, open GMSH window by executing command `make gmsh-edit`. In order to tune mesh size, do the following in a loop:

1. Mesh model by pressing `Modules->Mesh->3D` (shortcut key <kbd>3</kbd>)
2. Inspect produced mesh
3. Adjust mesh element sizes in `GMESH_CONFIG_FILE`
4. Reload script by pressing <kbd>9</kbd>

#### Variables

* `GMSH_STEP`: name of the input `.STEP` file. Default: `$(GEOM_BASENAME).step`
* `GMESH_FILE`: GMSH script file. Default: `$(GEOM_BASENAME).geo`
* `GMESH_CONFIG_FILE`: GMSH configuration file. Default: `$(GEOM_BASENAME)_config.geo`
* `MESH_FILE`: produced mesh file. Default: `$(GEOM_BASENAME).msh`
* `OPTIMIZE_NETGEN`: use netgen to optimize mesh. Default: `yes`
* `PARAVIEW_MESH_VOLUMES`: paraview mesh preview file. Default: `$(GEOM_BASENAME)_volumes.xdmf`
* `PARAVIEW_MESH_SURFACES`: paraview boundary condition preview file. Default: `$(GEOM_BASENAME)_surfaces.xdmf`

#### Commands

* `make gmsh-edit`: open model in GMSH
* `make gmsh-view-mesh`: open mesh in GMSH
* `make paraview-view-mesh`: open mesh and boundary conditions in paraview
* `make check-mesh`: check created mesh

### Project variable configuration

Most used project variables are stored in `config.mk` file. Edit relevant variables in `config.mk` and issue command `make setup-stationary` or `make setup-transient` to setup stationary or transient solution mode respectively.

#### Variables

* `inletFlowRate`: inlet mass flow rate [m^3/s]. Default: `1.6e-5`
* `outletPressure`: outlet pressure [P]. Default: `1e5`
* `coolerT`: cooler temperature [K]. Default: `300`
* `fixedT`: fixed boundary temperature [K]. Default: `30`
* `initialT`: initial temperature [K]. Default: `10`
* `gravity`: gravity vector (x,y,z) [m/s^2]. Default: `0 0 -9.81`
* `coordinatesUseGeometricCenter`: use geometric center or coordinate values switch (true|false). Default: `true`
* `coordinateX`: X coordinate of the heating region center (Gaussian function) [m]. Ignored when coordinatesUseGeometricCenter is true. Default: `0`
* `coordinateY`: Y coordinate of the heating region center (Gaussian function) [m]. Ignored when coordinatesUseGeometricCenter is true. Default: `0`
* `coordinateZ`: Z coordinate of the heating region center (Gaussian function) [m]. Ignored when coordinatesUseGeometricCenter is true. Default: `0`
* `sigmaX`: X axis standard deviation of the heating region (Gaussian function) [m]. Default: `0.001`
* `sigmaY`: Y axis standard deviation of the heating region (Gaussian function) [m]. Default: `0.001`
* `sigmaZ`: Z axis standard deviation of the heating region (Gaussian function) [m]. Default: `0.00025`
* `correctionFactor`: amplitude correction function. It is needed because Gaussian function is clipped by an application region. Default: `1`
* `heatSourceSaveToFile`: save heat source to file (true|false) (forced to false when PARALLEL=yes). Default: `true`
* `heatSourceFilePath`: heat source file path. Default: `heatSourceData.csv`
* `writeCount`: number of time steps to keep on disk. Default: `10`
* `power`: stationary power source [W]. Default: `10`
* `writeInteration`: save to disk each Nth iteration. Default: `10`
* `endIteration`: end of calculation iteration number. Default: `5000`
* `energy`: energy of beam in transient mode [eV]. Default: `1e+07`
* `current`: current of beam in transient mode [A]. Default: `0.1`
* `frequency`: beam impulse frequency [Hz]. Default: `1`
* `impulseLength`: beam impulse length [s]. Default: `1e-05`
* `timeStepOn`: transient solution time step on impulse [s]. Default: `1e-06`
* `timeStepOff`: transient solution time step off impulse [s]. Default: `1e-03`
* `writeInterval`: save to disk each Nth timestep. Default: `1e-05`
* `endTime`: calculation end time [s]. Default: `1e-3`

#### Commands

* `make setup-stationary`: set project variables and prepare stationary solution mode
* `make setup-transient`: set project variables and prepare transient solution mode
* `make heat-source-options`: view heat source options
* `make setup-bc`: set boundary condition values. This command is executed automatically and normally not needed
* `make setup-setup-heatsource`: set heat source values. This command is executed automatically and normally not needed

### Solution

Run `make solve` to solve the problem. If parallel execution is required, configure model splitting in `system/decomposeParDict`. Each model chunk will be solved on in separate MPI process.

#### Supplied heat

`make solve` command shows only iteration/time step and execution time. Full solver output is located in `LOGNAME` file.
It is recommended to check supplied heater value by examining log file:
```bash
grep 'Adding heat source' target.log
```
If it does not match the required value, adjust the `correctionFactor`.

#### Runtime summary values

Some summary values are calculated during solution. They are located in `postProcessing` folder.
For example, to visualize heater temperature in realtime use helper script
```bash
./scripts/plotTSV.py postProcessing/heater/fieldMinMaxHeater/0/fieldMinMax.dat 2
```
This script plots second column of the `fieldMinMax.dat` `.TSV` file as it updates.

#### Variables

* `PARALLEL`: use MPI to run simulation in parallel mode (yes|no). Default: `no`
* `LOGNAME`: solution log file name. Default: `$(GEOM_BASENAME).log`

#### Commands

* `make solve`: solve problem

### Post-processing

Run `make paraview` to visualize the solution using paraview.
It is possible to save visualization configuration in `PARAVIEW_STATE`.

#### Selecting plotting domain

![domain](.doc/paraview_T.png)

Upon loading, only imported solution file (**1**) is present in pipeline browser.
Block extractor tool (**2**) may be used to select geometry subdomains.
It is displayed in pipeline hierarchy (**3**) and configured in properties window (**4**).
After selection configuration, unit (**5**) and display style (**6**) may be selected.
In static state visualization, the latest iteration number must be selected (**7**).

#### Cooler flow

![cooler](.doc/paraview_U.png)

For cooler flow visualization, stream tracer (**1**) may be used.
It is displayed in pipeline hierarchy (**2**) and configured in properties window (**3**).
After selection configuration, vector unit (**4**) may be selected.
For better visualization, outline display (**4**) of the cooler domain may be displayed.

#### Plotting over line

![line](.doc/paraview_line.png)

Line plot tool (**1**) allows plotting 2D charts over the specified line.
It is displayed in pipeline hierarchy (**2**).
Line coordinates are configured in properties window (**3**).
After configuration, 2D chart will be shown in separate window.

#### Transitional properties

Some values are calculated during solution and stored in `postProcessing` folder.
They are configured in `system/controlDict` file.
For example, entry `fieldMinMaxHeater` in `system/controlDict` calculates minimum and maximum heater temperatures over
time.
The results are stored in file `postProcessing/heater/fieldMinMaxHeater/0/fieldMinMax.dat`.

#### Variables

* `PARAVIEW_STATE`: saved paraview state. Default: `$(GEOM_BASENAME).pvsm`

#### Commands

* `make solve`: solve problem
