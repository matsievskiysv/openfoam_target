#!/usr/bin/env freecadcmd

from os import environ
import FreeCAD
import Import

FreeCAD.openDocument(environ["FREECAD_FILE"])

Import.export([FreeCAD.getDocument(environ["FREECAD_DOCUMENT"])
               .getObjectsByLabel(label)[0] for label in environ["FREECAD_BODIES"].split(" ")],
              environ["GMSH_STEP"])
