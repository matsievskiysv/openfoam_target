#!/usr/bin/env python3

from sys import argv, exit
from os import _exit
from time import sleep
import numpy as np
import pylab as plt


def main():
    try:
        data = np.genfromtxt(argv[1])
    except Exception:
        exit('Cannot load TSV file')

    try:
        row = int(argv[2])
    except Exception:
        exit('Cannot convert argument to number')

    plt.ion()

    figure, ax = plt.subplots(2, 1)
    line1, = ax[0].plot(data[:, 0], data[:, row])
    line2, = ax[1].plot(data[:-1, 0], np.abs(np.diff(data[:, row])))
    ax[0].set_xlabel('Time [s] / Iteration')
    ax[0].set_ylabel('Property')
    ax[0].grid()
    ax[1].set_yscale('log')
    ax[1].set_xlabel('Time [s] / Iteration')
    ax[1].set_ylabel('Property delta')
    ax[1].grid()
    plt.tight_layout()

    while True:
        try:
            data = np.genfromtxt(argv[1])
        except:
            exit('Cannot load TSV file')
        line1.set_xdata(data[:,0])
        line1.set_ydata(data[:,row])
        line2.set_xdata(data[:-1,0])
        line2.set_ydata(np.abs(np.diff(data[:,row])))
        xr = np.max(data[:,0]) - np.min(data[:,0])
        yr = np.max(data[:,row]) - np.min(data[:,row])
        ax[0].set_xlim(np.min(data[:,0]) - xr * 0.05, np.max(data[:,0]) + xr * 0.05)
        ax[0].set_ylim(np.min(data[:,row]) - yr * 0.05, np.max(data[:,row]) + yr * 0.05)
        ax[1].set_xlim(np.min(data[:,0]) - xr * 0.05, np.max(data[:,0]) + xr * 0.05)
        ax[1].set_ylim(np.min(1e-10 + np.abs(np.diff(data[:,row]))) * 0.95, np.max(np.abs(np.diff(data[:,row]))) * 1.05)
        figure.canvas.draw()
        figure.canvas.flush_events()
        sleep(1)


if __name__ == '__main__':
    if len(argv) < 2:
        exit('Usage: ./plotTSV.py <file> [<column>]')
    try:
        main()
    except KeyboardInterrupt:
        try:
            exit(0)
        except SystemExit:
            _exit(0)
