#!/usr/bin/env pvbatch

from sys import exit, argv

if len(argv) != 2:
    exit('Usage: ./save_views.py <state.pvsm>')

from pathlib import Path
state_file = Path(argv[1])
if not state_file.exists():
    exit(f'{state_file} does not exist')
state_file = state_file.absolute()

from paraview.simple import *
import paraview.servermanager as servermanager

LoadState(f'{state_file}',
          DataDirectory=f'{state_file.parent.absolute()}')

for first, source in paraview.simple.GetSources().items():
    name = first[0]
    if type(source).__name__ == "PlotOverLine":
        SaveData(f"{name}.csv", source)

for first, layout in GetLayouts().items():
    name = first[0]
    for num, view in enumerate(GetViewsInLayout(layout)):
        if type(view).__name__ == "RenderView":
            SetActiveView(view)
            Render()
            ResetCamera()
            cam = view.GetActiveCamera()
            cam.Dolly(1.6)
            [(Show(s).RescaleTransferFunctionToDataRange())
             for s in GetSources().values()
             if servermanager.GetRepresentation(s, view)
             is not None and GetDisplayProperties(s, view).Visibility==1]
            view.ViewSize = (1920, 1080)
            Render()
            SaveScreenshot(f"{name}-{num}.png", view,
                           ImageResolution=(1920, 1080))
