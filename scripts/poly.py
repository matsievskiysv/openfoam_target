#!/usr/bin/env python3

from sys import argv, exit

if len(argv) != 3:
    print("Usage: ./poly.py <csv data> <degree>")
    exit(1)

data_file = argv[1]
degree = int(argv[2])

if degree > 7:
    print("OpenFOAM supports polynomials with degree up to 7")
    exit(1)

import numpy as np
import pylab

data = np.genfromtxt(data_file, delimiter=',')
rv = np.polyfit(data[:, 0], data[:, 1], degree)
fit_x = np.linspace(data[0, 0], data[-1, 0])
data_fit = np.polyval(rv, fit_x)
poly = np.zeros((8))
poly[:rv.shape[0]] = rv
print("(" + " ".join([f"{x:.5e}" for x in poly]) + ")")
pylab.plot(data[:, 0], data[:, 1], label="original")
pylab.plot(fit_x, data_fit, label=f"fit(degree={degree})")
pylab.grid()
pylab.legend()
pylab.show()
