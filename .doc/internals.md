# Electron accelerator target OpenFOAM calculation template

This is a template for calculation of electron accelerator target cooling. It uses the following programs:
  * [OpenFOAM](https://develop.openfoam.com/Development/openfoam/-/wikis/home) for thermodynamic-hydrodynamic coupled problem solution;
  * [FreeCAD](https://www.freecadweb.org) for target modeling;
  * [GMSH](https://gmsh.info/) for model meshing;
  * [ParaView](https://www.paraview.org/) for solution visualization;
  * [GNU Make](https://www.gnu.org/software/make/) for running commands in specified order.

Operation system is [Debian](https://www.debian.org/).

## Note about GNU Make

GNU Make is a program that allows creating recipes for creating files. It automatically prepares dependencies for the target file using specified rules. The general syntax of a Makefile rule is
```
<target>: <sources>
<literal tab character><command>
```
When issuing `make <target>` command, `make` would check the target sources, and if they are more recent then the target, execute the command. `make` does this for the whole dependency tree. Thus, changing any of the source files in the dependency tree would result in target update.

This approach is superior to simple shell scripting and suites well this application.

In case of problems adding `-n` switch to `make` command may help understand, what is going on. It will instruct make to print commands without evaluating them.

## Program installation

Most of the programs are installed using the following line:
```bash
sudo apt update && sudo apt install make gmsh freecad paraview python3-meshio
```

### OpenFOAM

At the moment of writing OpenFOAM Debian package didn't allow using `scalarCodedSourceCoeffs` function, necessary for distributed heating source definition, so we install OpenFOAM from sources.

Install build prerequisites:
```bash
sudo apt update && sudo apt build-dep openfoam
```

We will use stable OpenFOAM version:
```bash
mkdir -p $HOME/OpenFOAM && cd $HOME/OpenFOAM
git clone https://develop.openfoam.com/Development/openfoam.git --branch OpenFOAM-v2312 && cd openfoam
. ./etc/bashrc
```

OpenFOAM needs its environment to be set each time the program is used. In order to avoid setting it manually every time, issue the command
```bash
echo ". $(realpath ./etc/bashrc)" >> $HOME/.profile
```

Before start, check your system:
```bash
foamSystemCheck
```
This command's output should contain the line `System check: PASS`.

Start build:
```bash
foam
time ./Allwmake -j $(nproc)
```

To test installation, issue the command:
```bash
foamInstallationTest
```

**Note:** another installation instruction is available at [openFOAM repository](https://develop.openfoam.com/Development/openfoam/blob/develop/doc/Build.md).

## Geometry preparation

We used using FreeCAD to create a target model. It consists of copper enclosure, tungsten target plate and a water region. We will not go into detail about using FreeCAD because any 3D modeler capable of exporting STEP may be used in its place.

![model](model_freecad.png)
![water](model_freecad_water.png)

### Makefile rule

Makefile rule for exporting FreeCAD file to `.STEP` is
```makefile
$(GMSH_STEP): $(FREECAD_FILE)
	$(FREECADCMD) scripts/freecad_export.py
```
It is controlled by variables
```makefile
FREECAD_FILE?=$(GEOM_BASENAME).FCStd
FREECAD_DOCUMENT?=target
FREECAD_BODIES?=shell cooler heater
FREECADCMD?=freecadcmd
```

To trigger its evaluation, issue the command
```bash
make target.step
```

## Mesh preparation

In this step we need to do the following things:

* import geometry in GMSH;
* remove extra surfaces;
* set names to boundaries for application of boundary conditions in OpenFOAM.

When importing a new geometry, the old boundary numbers do not match the new ones, obviously. Therefore, GMSH script require reconfiguration when the new geometry is imported.
`make gmsh_edit` will open the new GMSH window with our `.STEP` file. In this file commands have the following meaning:

* select meter as a length unit
  ```gmsh
  Geometry.OCCTargetUnit = "M";
  ```
* select meshing algorithm
  ```gmsh
  Mesh.Algorithm=2; // 1: MeshAdapt, 2: Automatic, 3: Initial mesh only, 5: Delaunay, 6: Frontal-Delaunay, 7: BAMG, 8: Frontal-Delaunay for Quads, 9: Packing of Parallelograms
  Mesh.Algorithm3D=1; // 1: Delaunay, 3: Initial mesh only, 4: Frontal, 7: MMG3D, 9: R-tree, 10: HXT
  Mesh.Format=1; // 1: msh, 2: unv, 10: auto, 16: vtk, 19: vrml, 21: mail, 26: pos stat, 27: stl, 28: p3d, 30: mesh, 31: bdf, 32: cgns, 33: med, 34: diff, 38: ir3, 39: inp, 40: ply2, 41: celum, 42: su2, 47: tochnog, 49: neu, 50: matlab
  Mesh.RecombinationAlgorithm=1; // 0: simple, 1: blossom, 2: simple full-quad, 3: blossom full-quad
  ```
* select minimum and maximum mesh sizes
  ```gmsh
  Mesh.MeshSizeMin = 0.00002;
  Mesh.MeshSizeMax = 0.0002;
  ```
* import `.STEP` file and remove extra surfaces
  ```gmsh
  v[] = ShapeFromFile(target);
  BooleanFragments{ Volume{:}; Delete; }{}
  Recursive Delete { Surface{:}; }
  ```
* assign names to the volumes. These names will be used by OpenFOAM to name volume to volume boundaries
  ```gmsh
  Physical Volume("shell") = {v[0]};
  Physical Volume("cooler") = {v[1]};
  Physical Volume("heater") = {v[2]};
  ```
* assign names to the surfaces. These names are also imported by OpenFOAM
  ```gmsh
  Physical Surface("inlet") = {18};
  Physical Surface("outlet") = {20};
  Physical Surface("temperature") = {6, 8};
  ```

GMSH script requires assigning proper surface and volume indices to their names. Probably the best way to learn these indices is to open `Visibility` window by clicking `Tools->Visibility` and select some indices in `Tree` tab. This will instruct GMSH to only show selected indices. Note, that multiple indices may be included in a group. After `.GEO` script modification press `9` for GMSH to source it.

![gmsh](gmsh_indices.png)

### Makefile rule

Makefile rule for opening GMSH window is
```makefile
gmsh_edit: $(GMSH_STEP)
	$(GMSH) -setstring target $(GMSH_STEP) -setstring user_config $(GMESH_CONFIG_FILE) $(GMESH_FILE)
```

To open GMSH window with `.GEO` script sourced, issue the command
```bash
make gmsh_edit
```

Makefile rule for generating `.MSH` file is
```makefile
$(MESH_FILE): $(GMSH_STEP) $(GMESH_FILE) $(GMESH_CONFIG_FILE)
	$(GMSH) -setstring target $(GMSH_STEP) -setstring user_config $(GMESH_CONFIG_FILE) \
		$(GMESH_FILE) -3 $(OPTIMIZE_NETGEN) -nt $$(nproc) -o $(MESH_FILE)
```

To trigger `.MSH` file creation, issue the command
```bash
make target.msh
```

All these rules are controlled by variables
```makefile
GMSH?=gmsh
GMSH_STEP?=$(GEOM_BASENAME).step
GMESH_FILE?=$(GEOM_BASENAME).geo
GMESH_CONFIG_FILE=$(GEOM_BASENAME)_config.geo
MESH_FILE?=$(GEOM_BASENAME).msh
OPTIMIZE_NETGEN=yes
```

Makefile rule for created mesh visualization
```makefile
gmsh_view_mesh:
	$(GMSH) $(MESH_FILE)
```

To view generated mesh, issue the command
```bash
make gmsh_view_mesh
```

Makefile rule for created mesh visualization via paraview
```makefile
$(PARAVIEW_MESH_VOLUMES): $(MESH_FILE)
	./scripts/convert_msh.py $^ $@ tetra

$(PARAVIEW_MESH_SURFACES): $(MESH_FILE)
	./scripts/convert_msh.py $^ $@ triangle

paraview_view_mesh: $(PARAVIEW_MESH_VOLUMES) $(PARAVIEW_MESH_SURFACES)
	$(PARAVIEW) $^
```

To view generated mesh in paraview, issue the command
```bash
make paraview_view_mesh
```

## OpenFOAM

Here we will give only a brief overview of the OpenFOAM settings. OpenFOAM's [user guide](https://www.openfoam.com/documentation/guides/latest/doc/index.html) is the best resource to learn about specific settings.
Its `Home` section contains information related to the project configuration and `API` section has information about its C++ internals.

### Boundary names

When importing GMSH mesh to OpenFOAM:

* volumes will get the names assigned in GMSH;
* named surfaces will get the names assigned in GMSH;
* unnamed surfaces shared between two volumes will get produce two boundaries with names `<volume 1>_to_<volume_2>` and `<volume 2>_to_<volume_1>`;
* other unnamed surfaces will be added to `defaultFaces` patch.

### Directory structure

The following directories are the part of the OpenFOAM project
```
├── 0.orig
├── constant
│   ├── shell
│   ├── heater
│   └── cooler
└── system
    ├── shell
    ├── heater
    └── cooler
```

* `0.orig` will be copied to `0` and contains initial boundary conditions;
* `constant` folder contains solid and fluid properties. Its sub-folders have names of the volumes, as they were assigned in GMSH;
* `system` folder contains general solver and model settings. Some of these properties, which are applicable only to the specified volumes, are located in the respective sub-folders.

#### `0.orig`

`0.orig` folder contains initial conditions for the calculations. Prior to calculation, its contents is copied to folder `0`. Files in this folder represent scalar or vector fields

```
├── alphat
├── k
├── nut
├── omega
├── p
├── p_rgh
├── T
└── U
```

Most of these fields have placeholder definition
```c
internalField	uniform 0;

boundaryField
{
	".*"
	{
		type	calculated;
		value	$internalField;
	}
}
```
This definition tells OpenFOAM that this field value will be calculated based on other field values.

##### Initial temperature

`T` file contains the definition
```c
internalField	uniform 300;

boundaryField
{
	defaultFaces
	{
		type	zeroGradient;
	}
	temperature
	{
		type	fixedValue;
		value	$internalField;
	}
	".*"
	{
		type	calculated;
		value	$internalField;
	}
}
```
It sets constant temperature to the face with the name `temperature`, assigned in GMSH. Unnamed external faces from patch `defaultFaces` are set to `zeroGradient`.

##### Initial speed

`U` file contains the definition
```c
internalField	uniform (0 0 0);

boundaryField
{
	defaultFaces
	{
		type	zeroGradient;
	}
	".*"
	{
		type	calculated;
		value	$internalField;
	}
}
```
It sets zero speed at external boundaries.

#### `constant`

`0.orig` folder contains material properties and some other constants. Because some of the properties are shared between the volumes, common properties are stored in the root of the folder and symlinked to the volume folder files.

```
├── ./shell
│   ├── ./shell/fvOptions
│   ├── ./shell/radiationProperties -> ../radiationProperties
│   └── ./shell/thermophysicalProperties -> ../thermophysicalPropertiesCopper
├── ./g
├── ./radiationProperties
├── ./regionProperties
├── ./thermophysicalPropertiesCopper
├── ./thermophysicalPropertiesTungsten
├── ./thermophysicalPropertiesTungstenRhenium20
├── ./thermophysicalPropertiesWater
├── ./thermophysicalPropertiesWNiCu
├── ./heater
│   ├── ./heater/fvOptions
│   ├── ./heater/radiationProperties -> ../radiationProperties
│   └── ./heater/thermophysicalProperties -> ../thermophysicalPropertiesTungsten
└── ./cooler
    ├── ./cooler/fvOptions
    ├── ./cooler/radiationProperties -> ../radiationProperties
    ├── ./cooler/thermophysicalProperties -> ../thermophysicalPropertiesWater
    └── ./cooler/turbulenceProperties
```

##### `regionProperties`

This file assigns material type to the volumes
```c
regions
(
    fluid       (cooler)
    solid       (shell heater)
);
```

##### `g`

`g` defines Earth gravity

```c
value		(9.81 0 0);
```
This vector should set to the appropriate direction. If you wish to disable gravity, set value to some negligibly small value, as setting it to zero sometimes confuses the solver.

##### `thermoPhysicalProperties`

`thermoPhysicalProperties` defines physical properties of the materials. Some properties are defined as polynomials with degree up to eight. There's a convenience script `scripts/poly.py` for fitting polynomials to `.CSV` data, that visualizes fitting curve and prints polynomial coefficients in the appropriate format.

###### Solids

```c
thermoType
{
	type			heSolidThermo;
	mixture			pureMixture;
	transport		polynomial;
	thermo			hPolynomial;
	equationOfState	rhoConst;
	specie			specie;
	energy			sensibleEnthalpy;
}

mixture
{
	specie
	{
		molWeight	63.546; // molar weight, [kg/kmol]
	}

	transport
	{
		kappaCoeffs<8>	(4.23735e+02 -7.19776e-02 0 0 0 0 0 0); // thermal conductivity, [W/(m.K)]
	}

	thermodynamics
	{
		Hf		0; // enthalpy of formation, [J/kg]
		Sf		0; // Standard entropy [J/kg/K]
		CpCoeffs<8>	(3.21236e+02 1.83912e-01 -4.82060e-05 0 0 0 0 0); // heat capacity at constant pressure, [J/(kg K)]
	}

	equationOfState
	{
		rho		8920; // density, [kg/m^3]
	}
}
```

###### Fluids

```c
thermoType
{
	type			heRhoThermo;
	mixture			pureMixture;
	transport		const;
	thermo			hConst;
	equationOfState	rhoConst;
	specie			specie;
	energy			sensibleEnthalpy;
}

mixture
{
	specie
	{
		molWeight	18;
	}
	equationOfState
	{
		rho		1000;
	}
	thermodynamics
	{
		Cp		4181;
		Hf		0;
	}
	transport
	{
		mu		959e-6;
		Pr		6.62;
	}
}
```

##### `turbulenceProperties`

Fluids have one extra file called `turbulenceProperties` that defines selected turbulence model.

```c
simulationType	RAS;

RAS
{
	RASModel	kOmegaSST;
	turbulence	on;
	printCoeffs	on;
}
```

##### `fvOptions`

File `fvOptions` stands for Finite Volume Options and contains many important options.
For volumes without internal heating this file simply limits temperature and velocity (for fluids) for solution stability

```c
limitT
{
	type		limitTemperature;
	active		yes;
	selectionMode	all;
	min		300;
	max		5000;
}

limitU
{
	type		limitVelocity;
	active		yes;
	selectionMode	all;
	max		15;
}
```

`scalarCodedSource` section allows us to simulate distributed heat source.

```c
customHeatSource
{
    type            scalarCodedSource;
    name            heatSource;
    active          true;
    heatSourceOptions
    {
        solutionStationary true;
        coordinatesUseGeometricCenter true;
        saveToFile      true;
        filePath        "heatSourceData.csv";
        coordinateX     0;
        coordinateY     0;
        coordinateZ     0;
        sigmaX          0.001;
        sigmaY          0.001;
        sigmaZ          0.001;
        correctionFactor 1;
        stationary
        {
            power           1;
        }
        transient
        {
            energy          1e+06;
            current         0.001;
            frequency       500;
            impulseLength   1e-06;
        }
    }
    scalarCodedSourceCoeffs
    {
        selectionMode   all;
        fields          ( h );
        codeInclude     #{
            #include <fstream>
            #include <unistd.h>
                        #};
        codeCorrect     #{ #};
        codeAddSupRho   #{
            const scalar time = mesh().time().value();
            const scalarField& V = mesh_.V();
            const vectorField& C = mesh_.C();
            scalarField& heSource = eqn.source();
            scalar amp = 0;
            scalar heSourceTot = 0;
            std::ofstream outdata;
            const bool firstIteration = access(".firstIteration", F_OK) == 0;

            // read options
            const auto heatSourceOptions = Foam::fv::option::dict_.findEntry("heatSourceOptions",
                                                                             keyType::LITERAL)->dict();
            const auto heatSourceStationary = heatSourceOptions.findEntry("stationary",
                                                                          keyType::LITERAL)->dict();
            const auto heatSourceTransient = heatSourceOptions.findEntry("transient",
                                                                         keyType::LITERAL)->dict();
            const bool solutionStationary = heatSourceOptions.findEntry("solutionStationary",
                                                                        keyType::LITERAL)->get<bool>();
            const bool coordinatesUseGeometricCenter = heatSourceOptions.findEntry("coordinatesUseGeometricCenter",
                                                                                   keyType::LITERAL)->get<bool>();
            const bool saveToFile = heatSourceOptions.findEntry("saveToFile",
                                                                keyType::LITERAL)->get<bool>();
            const Foam::string filePath = heatSourceOptions.findEntry("filePath",
                                                                      keyType::LITERAL)->get<Foam::string>();
            scalar coordinateX = heatSourceOptions.findEntry("coordinateX",
                                                             keyType::LITERAL)->get<scalar>();
            scalar coordinateY = heatSourceOptions.findEntry("coordinateY",
                                                             keyType::LITERAL)->get<scalar>();
            scalar coordinateZ = heatSourceOptions.findEntry("coordinateZ",
                                                             keyType::LITERAL)->get<scalar>();
            const scalar sigmaX = heatSourceOptions.findEntry("sigmaX",
                                                              keyType::LITERAL)->get<scalar>();
            const scalar sigmaY = heatSourceOptions.findEntry("sigmaY",
                                                              keyType::LITERAL)->get<scalar>();
            const scalar sigmaZ = heatSourceOptions.findEntry("sigmaZ",
                                                              keyType::LITERAL)->get<scalar>();
            const scalar correctionFactor = heatSourceOptions.findEntry("correctionFactor",
                                                                        keyType::LITERAL)->get<scalar>();
            const scalar power = heatSourceStationary.findEntry("power",
                                                                keyType::LITERAL)->get<scalar>();
            const scalar energy = heatSourceTransient.findEntry("energy",
                                                                keyType::LITERAL)->get<scalar>();
            const scalar current = heatSourceTransient.findEntry("current",
                                                                 keyType::LITERAL)->get<scalar>();
            const scalar frequency = heatSourceTransient.findEntry("frequency",
                                                                   keyType::LITERAL)->get<scalar>();
            const scalar impulseLength = heatSourceTransient.findEntry("impulseLength",
                                                                       keyType::LITERAL)->get<scalar>();
            if (coordinatesUseGeometricCenter) {
                    // calculate mean coordinate
                    coordinateX = 0;
                    coordinateY = 0;
                    coordinateZ = 0;
                    forAll (C, i) {
                            coordinateX += C[i].x();
                            coordinateY += C[i].y();
                            coordinateZ += C[i].z();
                    }
                    coordinateX /= C.size();
                    coordinateY /= C.size();
                    coordinateZ /= C.size();
            }

            if (solutionStationary) {
                amp = power;
            } else {
                amp = energy * current;
                const scalar T = 1 / frequency;
                const scalar func = fmod(time, T) / T;
                const scalar duty = impulseLength / T;
                amp = func <= duty ? amp : 0;
            }
            const scalar Amplitude = correctionFactor * amp / (std::pow(2*Foam::constant::mathematical::pi, 3.0/2.0) *
                                                               sigmaX * sigmaY * sigmaZ);

            if (saveToFile && firstIteration) {
                    outdata.open(filePath);
                    outdata << "x,y,z,data" << std::endl;
            }

            forAll (heSource, i) {
                    scalar x = -std::pow((C[i].x() - coordinateX) / sigmaX, 2) / 2.0;
                    scalar y = -std::pow((C[i].y() - coordinateY) / sigmaY, 2) / 2.0;
                    scalar z = -std::pow((C[i].z() - coordinateZ) / sigmaZ, 2) / 2.0;
                    heSource[i] -= Amplitude * V[i] * std::exp(x + y + z);
                    if (saveToFile && firstIteration) {
                            outdata << C[i].x() << "," << C[i].y() << "," << C[i].z() \
                                    << "," << - heSource[i] << std::endl;
                    }
                    heSourceTot -= heSource[i];
            }
            if (saveToFile && firstIteration) {
                    outdata.close();
                    unlink(".firstIteration");
            }
            Pout<< "Adding heat source: " << heSourceTot << " [W]  with center at (" \
                << coordinateX << ";" << coordinateY << ";" << coordinateZ << ")" << endl;
                        #};
        codeAddSup      #{ #};
        codeConstrain   #{ #};
    }
}
```

In this project Gaussian heat source is used.

$$
f_i = \frac{k A V_i}{\left(2\pi\right)^{3/2}\sigma_x\sigma_y\sigma_z}
\exp{\left\{
-\frac{1}{2}\left(\frac{x_i - x_0}{\sigma_x}\right)^2
-\frac{1}{2}\left(\frac{y_i - y_0}{\sigma_y}\right)^2
-\frac{1}{2}\left(\frac{z_i - z_0}{\sigma_z}\right)^2
\right\}}
$$

For the stationary case $A$ is simply a total power transmitted to the volume. For the transient case rectangle function is used
$$A=E I \begin{cases}
1,\quad \left(t\%T\right)/T\le D\\
0,\quad \operatorname{else}
\end{cases}$$

It is controlled by the inner dictionary `heatSourceOptions`. Its fields have meaning:

* `solutionStationary` switches heat source between stationary (`true`) and transient (`false`) modes;
* when `coordinatesUseGeometricCenter` is set to `true`, center of the Gaussian function is assigned to the geometric center of the volume; when it is set to `false`, fields `coordinateX`, `coordinateY` and `coordinateZ` contain the coordinate of the Gaussian function center $(x_0, y_0, z_0)$;
* `sigmaX` ($\sigma_x$), `sigmaY` ($\sigma_y$) and `sigmaZ` ($\sigma_z$) control the shape of the Gaussian function;
* `correctionFactor` $k$ is used to take into account Gaussian function clipping; it is only needed when values of $\sigma$ are too big;
* in stationary case `power` sets supplied power in Watts;
* in transient case `energy` and `current` describe beam energy parameters; `impulseLength` is an impulse length in seconds and `frequency` is a beam repetition frequency in Hertz;
* when `saveToFile` is set to `true`, heat distribution is written into the `filePath` file in `.csv` format (see `Heat source visualization` section for details);


#### `system`

`system` folder contains solver properties and initial value overrides.

```
├── changeDictionaryDict
├── controlDict
├── shell
│   ├── changeDictionaryDict
│   ├── decomposeParDict -> ../decomposeParDict
│   ├── fvSchemes -> ../fvSchemesSolid
│   └── fvSolution -> ../fvSolutionSolid
├── decomposeParDict
├── fvSchemes
├── fvSchemesFluid
├── fvSchemesSolid
├── fvSolution
├── fvSolutionFluid
├── fvSolutionSolid
├── heater
│   ├── changeDictionaryDict
│   ├── decomposeParDict -> ../decomposeParDict
│   ├── fvSchemes -> ../fvSchemesSolid
│   └── fvSolution -> ../fvSolutionSolid
└── cooler
    ├── changeDictionaryDict
    ├── decomposeParDict -> ../decomposeParDict
    ├── fvSchemes -> ../fvSchemesFluid
    └── fvSolution -> ../fvSolutionFluid
```

##### `decomposeParDict`

`decomposeParDict` is used for parallel calculation. It instructs OpenFOAM how to split subdomains for parallel calculation. It is not currently used because parallel calculation does not work with `scalarCodedSourceCoeffs`.

```
numberOfSubdomains	8;
method			simple;

regions
{
	heater
	{
		numberOfSubdomains	1;
		method			simple;

		coeffs
		{
			n		(1 1 1);
		}
	}
	shell
	{
		numberOfSubdomains	4;
		method			simple;

		coeffs
		{
			n		(2 1 2);
		}
	}
	cooler
	{
		numberOfSubdomains	3;
		method			simple;

		coeffs
		{
			n		(3 1 1);
		}
	}
}
```

#### `changeDictionaryDict`

`changeDictionaryDict` overrides initial value files in `0` folder using `changeDictionary` OpenFOAM utility. It's not strictly necessary to use `changeDictionaryDict` file instead of modifying files in `0.orig` folder directly. This simply allows keeping all the relevant information in a single file.

```
U
{
	internalField	uniform (0 0 0);

	boundaryField
	{
		inlet
		{
			type		flowRateInletVelocity;
			massFlowRate	0.06; // mass flow rate [kg/s]
			value		uniform (0 0 0);
		}
		outlet
		{
			type	zeroGradient;
		}
		"cooler_to_.*"
		{
			type	noSlip;
		}
	}
}

T
{
	boundaryField
	{
		inlet
		{
			type	fixedValue;
			value	uniform 300;
		}
		outlet
		{
			type	zeroGradient;
		}
		"cooler_to_.*"
		{
			type		compressible::turbulentTemperatureRadCoupledMixed;
			Tnbr		T;
			kappaMethod	fluidThermo;
			value		uniform 300;
		}
	}
}
```

#### `fvSchemes`

`fvSchemes` file defines Finite Volume schemes.

```
ddtSchemes
{
    default         steadyState;
}

gradSchemes
{
    default         Gauss linear;
}

divSchemes
{
    default         none;
    div(phi,U)      bounded Gauss linearUpwind grad(U);
    div(phi,K)      bounded Gauss limitedLinear 1;
    div(phi,h)      bounded Gauss limitedLinear 1;
    div(phi,k)      bounded Gauss limitedLinear 1;
    div(phi,epsilon) bounded Gauss limitedLinear 1;
    div(phi,omega)  bounded Gauss limitedLinear 1;
    div(phi,R)      bounded Gauss limitedLinear 1;
    div(R)          bounded Gauss limitedLinear 1;
    div(((rho*nuEff)*dev2(T(grad(U))))) Gauss linear;
}
```

#### `controlDict`

This is the main file for simulation control. `functions` entry defines calculation monitors. These are used to monitor simulation convergence in stationary calculations and to plot temperature values over time in transitional calculations.

```
meanTCooler
{
	type            volFieldValue;
	libs            ( fieldFunctionObjects );
	fields          ( T p U );
	operation       average;
	regionType      all;
	writeFields     true;
	writePrecision  8;
	writeToFile     true;
	region          cooler;
	enabled         true;
	log             false;
	executeControl  timeStep;
	executeInterval 1;
	writeControl    timeStep;
	writeInterval   1;
}
```

`vtkWrite` is used to control `.VTK` field export. It may be reconfigured to export only specified fields on selected regions.

```
vtkWrite
{
	type            vtkWrite;
	enabled         false;
	libs            ( utilityFunctionObjects );
	log             false;
	writeControl    always;
	regions         ( ".*" );
	internal        true;
	boundary        true;
	single          false;
	interpolate     true;
	fields          ( ".*" );
	format          binary;
	legacy          false;
	directory       "VTK";
	writeIds        false;
}
```

##### Stationary solution

Parameters for the stationary solver are

```
application     chtMultiRegionSimpleFoam;
startFrom       latestTime;
startTime       0;
stopAt          endTime;
endTime         3000;
deltaT          1;
writeControl    adjustable;
writeInterval   1;
purgeWrite      10;
writeFormat     binary;
writePrecision  8;
writeCompression off;
timeFormat      general;
timePrecision   6;
runTimeModifiable yes;
adjustTimeStep  no;
```

*Note:* for the stationary solver all `time` values are interpreted as iteration numbers. For example, `endTime 3000` means maximum iteration count is `3000`. Thus, `deltaT 1` is the only possible value for the stationary solver.

* `application`: for the stationary solution we use `chtMultiRegionSimpleFoam` solver
* `startFrom`: when solution is interrupted, it may start either from the `latestTime` or the specified time, if it was calculated
* `endTime`: number of solver iterations
* `writeControl`: if set to `adjustable`, OpenFOAM will reread `controlDict` file each iteration and adjust write settings accordingly
* `writeInterval`: dump full fields each Nth iteration
* `purgeWrite`: only keep last N solutions

##### Transitional solution

## Practical notes

### Heat source visualization

![paraview](paraview_heat_source.png)

In order to visualize heat source set `customHeatSource.heatSourceOptions.saveToFile` option to `true` by issuing the command
```bash
foamDictionary -set true -entry customHeatSource.heatSourceOptions.saveToFile constant/heater/fvOptions
```
or by editing `constant/heater/fvOptions` file manually.
Then, make at least one source iteration
```bash
make solve
```
and open paraview with command
```bash
make paraview
```

![paraview](.doc/paraview_heat_source.png)

This will open the latest solution in the paraview window (**1**).
Firstly, we need to select heater internal volume by adding `ExtractBlock` filter and checking `heater->internal` (**2**).
We need to set opacity to some lower value in order to see through the selected volume.
Secondly, we need to import `heatSourceData.csv` and convert it to points by using `TableToPoints` filter (**3**).
The resulting point cloud represents Finite Volume Method mesh elements with added heat values.
It may be sliced in half in order to better see the middle part of the volume.

