SHELL:=bash

# environment sentinel
ifndef WM_PROJECT_DIR
$(error WM_PROJECT_DIR variable is undefined. Source openFOAM environment)
endif

include config.mk

export FREECAD_FILE
export FREECAD_DOCUMENT
export FREECAD_BODIES

# gmsh settings
export GMSH_STEP

STASH_DIR=0.stash
ifeq ($(strip $(USE_STASH)),true)
INIT_DIR:=$(firstword $(wildcard $(STASH_DIR)) 0.orig)
else
INIT_DIR:=0.orig
endif

APPLICATION:=$(shell foamDictionary -value -entry application system/controlDict)
REGIONS:=$(shell foamListRegions)
REGIONS_SOLID:=$(shell foamListRegions solid)
REGIONS_FLUID:=$(shell foamListRegions fluid)
FIELD_FILES_ORIG:=$(wildcard $(INIT_DIR)/*)
FIELD_FILES:=$(foreach v,$(FIELD_FILES_ORIG),$(notdir $(v)))
FIELD_FILES_INITIAL:=$(foreach v,$(FIELD_FILES),0/$(v))
FIELD_FILES_INITIAL_REGIONS:=$(foreach v,$(FIELD_FILES),$(foreach r,$(REGIONS),0/$(r)/$(v)))

ifeq ($(strip $(TIME)),latest)
TIMECMD:=-latestTime
STASHTIME:=$(shell foamListTimes -latestTime)
else
TIMECMD:=-time $(TIME)
STASHTIME:=$(TIME)
endif

ifeq ($(strip $(PARALLEL)),yes)
heatSourceSaveToFile:=false
endif

ifeq ($(strip $(PARALLEL)),no)
define solver =
	rm -f $(heatSourceFilePath)
	$(APPLICATION) | tee $(LOGFILE)
endef
else
define solver =
	decomposePar -allRegions
	touch .parallel
	mpiexec --host localhost:$(NPROC) -n $(NPROC) $(APPLICATION) -parallel | tee $(LOGFILE)
endef
endif

ifeq ($(strip $(PARALLEL)),no)
define converter =
	foamToVTK -overwrite -allRegions $(TIMECMD)
endef
else
define converter =
	reconstructPar -allRegions $(TIMECMD)
	foamToVTK -overwrite -allRegions $(TIMECMD)
endef
endif

ifeq ($(strip $(OPTIMIZE_NETGEN)),no)
OPTIMIZE_NETGEN:=
else
OPTIMIZE_NETGEN:=-optimize_netgen
endif

$(GMSH_STEP): $(FREECAD_FILE)
	$(FREECADCMD) scripts/freecad_export.py

$(MESH_FILE): $(GMSH_STEP) $(GMESH_FILE) $(GMESH_CONFIG_FILE)
	$(GMSH) -setstring target $(GMSH_STEP) -setstring user_config $(GMESH_CONFIG_FILE) \
		$(GMESH_FILE) -3 $(OPTIMIZE_NETGEN) -nt $$(nproc) -o $(MESH_FILE)

.PHONY: gmsh-edit
##! open GMSH project
gmsh-edit: $(GMSH_STEP)
	$(GMSH) -setstring target $(GMSH_STEP) -setstring user_config $(GMESH_CONFIG_FILE) $(GMESH_FILE)

.PHONY: gmsh-view-mesh
##! visualize mesh in GMSH
gmsh-view-mesh: $(MESH_FILE)
	$(GMSH) $(MESH_FILE)

$(PARAVIEW_MESH_VOLUMES): $(MESH_FILE)
	./scripts/convert_msh.py $^ $@ tetra

$(PARAVIEW_MESH_SURFACES): $(MESH_FILE)
	./scripts/convert_msh.py $^ $@ triangle

.PHONY: paraview-view-mesh
##! visualize mesh in paraview
paraview-view-mesh: $(PARAVIEW_MESH_VOLUMES) $(PARAVIEW_MESH_SURFACES)
	$(PARAVIEW) $^

0:
	mkdir -p 0

0/%: $(INIT_DIR)/% | 0
	rm -rf $@
	cp -rv $^ $@

constant/polyMesh: $(MESH_FILE)
	gmshToFoam $(MESH_FILE)

.PHONY: check-mesh
##! check mesh
check-mesh: constant/polyMesh
	checkMesh -allGeometry -allTopology

0/cellToRegion: constant/polyMesh $(FIELD_FILES_INITIAL) | 0
	rm -f .changeDictionary
	splitMeshRegions -cellZones -overwrite
	renumberMesh -allRegions -overwrite

$(foreach r,$(REGIONS),constant/$(r)/polyMesh 0/$(r)/cellToRegion): 0/cellToRegion
$(FIELD_FILES_INITIAL_REGIONS): 0/cellToRegion

.changeDictionary:  0/cellToRegion $(FIELD_FILES_INITIAL_REGIONS) $(wildcard system/**/changeDictionaryDict)
ifeq ($(INIT_DIR),0.orig)
	$(foreach region,$(REGIONS),changeDictionary -region $(region);)
endif
	touch .changeDictionary

.PHONY: solve
##! solve problem
solve: .changeDictionary
	rm -f .parallel
	rm -f VTK/.convert
	rm -f 0/uniform/time
	$(solver)

.PHONY: show-solution-time
##! show solution time
show-solution-time:
	touch $(LOGFILE)
	tail --follow $(LOGFILE) | grep 'Time ='

VTK/.convert:
	$(converter)
	$(foreach file,$(PARAVIEW_STATE),\
		sed -i 's|/home/.*/VTK/|./VTK/|g' $(file); \
		find ./VTK -maxdepth 1 -iname '*.vtm.series' -exec sed -i 's|./VTK/.*vtm.series"|{}"|g' $(file) \; ; \
		find ./VTK -maxdepth 1 -iname '*.vtm' -exec sed -i 's|./VTK/.*vtm"|{}"|g' $(file) \; ; \
	)
	touch VTK/.convert

.PHONY: convert
##! convert results to paraview
convert: VTK/.convert

.PHONY: paraview
##! view solution
paraview: VTK/.convert
	$(PARAVIEW) $(firstword $(PARAVIEW_STATE) $(wildcard VTK/$(PROJECT)-regions.vtm.series))

.PHONY: heat-source-options
##! show heat source options
heat-source-options:
	foamDictionary -entry customHeatSource.heatSourceOptions constant/heater/fvOptions

.PHONY: setup-solver
##! setup solver settings
setup-solver:
	foamDictionary -set $(enableTurbulence) -entry RAS.turbulence constant/cooler/turbulenceProperties

.PHONY: setup-bc
##! setup boundary conditions
setup-bc:
	foamDictionary -set $(inletFlowRate) -entry U.boundaryField.inlet.volumetricFlowRate \
		system/cooler/changeDictionaryDict
	foamDictionary -set 'uniform $(initialT)' -entry T.internalField system/cooler/changeDictionaryDict
	foamDictionary -set 'uniform $(coolerT)' -entry T.boundaryField.inlet.value system/cooler/changeDictionaryDict
	foamDictionary -set 'uniform $(coolerT)' -entry 'T/boundaryField/"cooler_to_.*"/value' \
		system/cooler/changeDictionaryDict
	foamDictionary -set 'uniform $(outletPressure)' -entry p_rgh.internalField system/cooler/changeDictionaryDict
	foamDictionary -set 'uniform $(outletPressure)' -entry p_rgh.boundaryField.outlet.value \
		system/cooler/changeDictionaryDict
	foamDictionary -set 'uniform $(initialT)' -entry T.internalField system/heater/changeDictionaryDict
	foamDictionary -set 'uniform $(initialT)' -entry 'T/boundaryField/"heater_to_.*"/value' \
		system/heater/changeDictionaryDict
	foamDictionary -set 'uniform $(initialT)' -entry T.internalField system/shell/changeDictionaryDict
	foamDictionary -set 'uniform $(initialT)' -entry 'T/boundaryField/"shell_to_.*"/value' \
		system/shell/changeDictionaryDict
	foamDictionary -set 'uniform $(fixedT)' -entry boundaryField.fixed/value 0.orig/T
	foamDictionary -set '( $(gravity) )' -entry value constant/g

.PHONY: setup-heatsource
##! setup heat source options
setup-heatsource:
	foamDictionary -set $(coordinatesUseGeometricCenter) -entry \
		customHeatSource.heatSourceOptions.coordinatesUseGeometricCenter constant/heater/fvOptions
	foamDictionary -set $(coordinateX) -entry \
		customHeatSource.heatSourceOptions.coordinateX constant/heater/fvOptions
	foamDictionary -set $(coordinateY) -entry \
		customHeatSource.heatSourceOptions.coordinateY constant/heater/fvOptions
	foamDictionary -set $(coordinateZ) -entry \
		customHeatSource.heatSourceOptions.coordinateZ constant/heater/fvOptions
	foamDictionary -set $(sigmaX) -entry \
		customHeatSource.heatSourceOptions.sigmaX constant/heater/fvOptions
	foamDictionary -set $(sigmaY) -entry \
		customHeatSource.heatSourceOptions.sigmaY constant/heater/fvOptions
	foamDictionary -set $(sigmaZ) -entry \
		customHeatSource.heatSourceOptions.sigmaZ constant/heater/fvOptions
	foamDictionary -set $(correctionFactor) -entry \
		customHeatSource.heatSourceOptions.correctionFactor constant/heater/fvOptions
	foamDictionary -set $(heatSourceSaveToFile) -entry \
		customHeatSource.heatSourceOptions.heatSourceSaveToFile constant/heater/fvOptions
	foamDictionary -set \"$(heatSourceFilePath)\" -entry \
		customHeatSource.heatSourceOptions.heatSourceFilePath constant/heater/fvOptions
	foamDictionary -set $(power) -entry \
		customHeatSource.heatSourceOptions.stationary.power constant/heater/fvOptions
	foamDictionary -set $(current) -entry \
		customHeatSource.heatSourceOptions.transient.current constant/heater/fvOptions
	foamDictionary -set $(energy) -entry \
		customHeatSource.heatSourceOptions.transient.energy constant/heater/fvOptions
	foamDictionary -set $(frequency) -entry \
		customHeatSource.heatSourceOptions.transient.frequency constant/heater/fvOptions
	foamDictionary -set $(impulseLength) -entry \
		customHeatSource.heatSourceOptions.transient.impulseLength constant/heater/fvOptions

.PHONY: setup-stationary
##! setup stationary solver
setup-stationary: setup-heatsource setup-bc setup-solver
	foamDictionary -set chtMultiRegionSimpleFoam -entry application system/controlDict
	foamDictionary -set $(endIteration) -entry endTime system/controlDict
	foamDictionary -set $(writeCount) -entry purgeWrite system/controlDict
	foamDictionary -set $(writeInteration) -entry writeInterval system/controlDict
	foamDictionary -set 1 -entry deltaT system/controlDict
	foamDictionary -set no -entry adjustTimeStep system/controlDict
	foamDictionary -set steadyState -entry ddtSchemes.default system/fvSchemesFluid
	foamDictionary -set steadyState -entry ddtSchemes.default system/fvSchemesSolid
	foamDictionary -set true -entry customHeatSource.heatSourceOptions.solutionStationary constant/heater/fvOptions

.PHONY: setup-transient
##! setup transient solver
setup-transient: setup-heatsource setup-bc setup-solver
	foamDictionary -set chtMultiRegionFoam -entry application system/controlDict
	foamDictionary -set $(endTime) -entry endTime system/controlDict
	foamDictionary -set $(writeCount) -entry purgeWrite system/controlDict
	foamDictionary -set $(writeInterval) -entry writeInterval system/controlDict
	foamDictionary -set $(timeStepOn) -entry deltaT system/controlDict
	foamDictionary -set $(timeStepOn) -entry maxDeltaT system/controlDict
	foamDictionary -set $(maxCo) -entry maxCo system/controlDict
	foamDictionary -set yes -entry adjustTimeStep system/controlDict
	foamDictionary -set yes -entry runTimeModifiable system/controlDict
	foamDictionary -set Euler -entry ddtSchemes.default system/fvSchemesFluid
	foamDictionary -set Euler -entry ddtSchemes.default system/fvSchemesSolid
	foamDictionary -set false -entry customHeatSource.heatSourceOptions.solutionStationary \
		constant/heater/fvOptions
	foamDictionary -set $(timeStepOn) -entry customHeatSource.heatSourceOptions.transient.timestep.on \
		constant/heater/fvOptions
	foamDictionary -set $(timeStepOff) -entry customHeatSource.heatSourceOptions.transient.timestep.off \
		constant/heater/fvOptions

.PHONY: stash-solution
##! stash solution directory
stash-solution:
	rm -rf -- $(STASH_DIR)
	cp -rv $(STASHTIME) $(STASH_DIR)

.PHONY: drop-stash
##! remove stashed solution directory
drop-stash:
	rm -rf -- $(STASH_DIR)

.PHONY: clean
##! clear solution files
clean:
	find . -maxdepth 1 -type d -regex '.*/processor[0-9]+' -exec rm -r {} +
	foamListTimes -rm -noZero
	find . -type d -iname '.ccls-cache' -exec rm -r {} +
	find . -type d -name __pycache__ -exec rm -r {} +
	find . -type f \( -name "*.pyc" \) -delete
	find . -maxdepth 1 -type f \( -name "*.xdmf" \
	-o -name "*.h5" \
	\) -delete
	rm -f VTK/.convert
	rm -f .renumber
	rm -rf VTK
	rm -rf dynamicCode
	rm -rf postProcessing
	rm -f $(GEOM_BASENAME).log
	rm -f $(heatSourceFilePath)

.PHONY: distclean
##! clear preprocessing files
distclean: clean
	foamListTimes -rm -withZero
	find constant -maxdepth 2 -type d -name polyMesh -exec rm -r {} +
	rm -f constant/cellToRegion
	rm -f .changeDictionary

.PHONY: purge
##! clear all generated files
purge: distclean
	rm -f $(GMSH_STEP)
	rm -f $(MESH_FILE)
	rm -f $(PROJECT).zip
	rm -rf $(STASH_DIR)
	find . -maxdepth 1 -type f \( \
	-name "*.pvsm" \
	-o -name "*.FCStd1" \
	-o -name "*.FCBak" \
	\) -delete

.PHONY: zip
##! zip project
zip:
	zip -r --symlinks $(PROJECT).zip .

.DEFAULT_GOAL:=help

# https://gist.github.com/klmr/575726c7e05d8780505a
.PHONY: help
##! this message
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^##! / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^##! //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n##! /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| cat $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')
