shell_volumes = {1};
cooler_volumes = {2};
heater_volumes = {3};
inlet_surfaces = {38};
outlet_surfaces = {39};
fixed_surfaces = {8,16};

background_mesh_size = "0.0008";
cooler_mesh_size = "0.0008";
heater_mesh_size = "0.0008";
shell_mesh_size = "0.0008";
