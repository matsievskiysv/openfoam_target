SetFactory("OpenCASCADE");

Geometry.OCCTargetUnit = "M";
Mesh.Algorithm=2; // 1: MeshAdapt, 2: Automatic, 3: Initial mesh only, 5: Delaunay, 6: Frontal-Delaunay, 7: BAMG, 8: Frontal-Delaunay for Quads, 9: Packing of Parallelograms
Mesh.Algorithm3D=1; // 1: Delaunay, 3: Initial mesh only, 4: Frontal, 7: MMG3D, 9: R-tree, 10: HXT
Mesh.Format=1; // 1: msh, 2: unv, 10: auto, 16: vtk, 19: vrml, 21: mail, 26: pos stat, 27: stl, 28: p3d, 30: mesh, 31: bdf, 32: cgns, 33: med, 34: diff, 38: ir3, 39: inp, 40: ply2, 41: celum, 42: su2, 47: tochnog, 49: neu, 50: matlab
Mesh.RecombinationAlgorithm=1; // 0: simple, 1: blossom, 2: simple full-quad, 3: blossom full-quad

Mesh.MeshSizeFromPoints = 0;
Mesh.MeshSizeFromCurvature = 0;
Mesh.MeshSizeExtendFromBoundary = 0;

v[] = ShapeFromFile(target);

BooleanFragments{ Volume{:}; Delete; }{}
Recursive Delete { Surface{:}; }

Include Str(user_config);

Physical Volume("shell") = shell_volumes[];
Physical Volume("cooler") = cooler_volumes[];
Physical Volume("heater") = heater_volumes[];

Physical Surface("inlet") = inlet_surfaces[];
Physical Surface("outlet") = outlet_surfaces[];
Physical Surface("fixed") = fixed_surfaces[];

//background
Field[1] = MathEval;
Field[1].F = Str(background_mesh_size);

// cooler
Field[2] = MathEval;
Field[2].F = Str(cooler_mesh_size);
Field[3] = Restrict;
Field[3].InField = 2;
Field[3].VolumesList = {cooler_volumes[]};
Field[3].IncludeBoundary = 1;

// heater
Field[4] = MathEval;
Field[4].F = Str(heater_mesh_size);
Field[5] = Restrict;
Field[5].InField = 4;
Field[5].VolumesList = {heater_volumes[]};
Field[5].IncludeBoundary = 1;

// shell
Field[6] = MathEval;
Field[6].F = Str(shell_mesh_size);
Field[7] = Restrict;
Field[7].InField = 6;
Field[7].VolumesList = {shell_volumes[]};
Field[7].IncludeBoundary = 1;

// union
Field[8] = Min;
Field[8].FieldsList = {1,3,5,7};
Background Field = 8;
